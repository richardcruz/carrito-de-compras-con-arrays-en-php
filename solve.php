<!DOCTYPE html>
<html>
<head>
	<title>CARRITO DE COMPRAS </title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/font-awesome/css/fontawesome-all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
</head>

<body>
	<div class="contenedor">
		<!--INICIO Menu de Navegacion-->
		<nav class="iconos">
			<ul>
				<li>
					<a href="index.html">
						<div class="icono">
							<i class="fas fa-home"></i> <!--Etiqueta para icono-->
						</div>
						<div class="texto">
							Inicio
							<span>Página Principal</span>
						</div>
					</a>
				</li>
				<li>
					<a href="carritoC.php">
						<div class="icono">
							<i class="fas fa-envelope"></i>
						</div>
						<div class="texto">
							Tienda Virtual
							<span>Adquire nuestros artículos</span>
						</div>
					</a>
				</li>
				<li>
					<a href="comprobante.php">
						<div class="icono">
							<i class="fas fa-dollar-sign"></i>
						</div>
						<div class="texto">
							Ver Carrito
							<span>Tus productos</span>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icono">
							<i class="fas fa-question-circle"></i>
						</div>
						<div class="texto">
							Asistencia
							<span>¿Necesitas Ayuda?</span>     
						</div>
					</a>
				</li>
			</ul>
		</nav>
		<!--FIN Menu de Navegacion-->

		<div class="contenedor catalogo">
			<main class="contenido-principal">
			<?php 
				session_start(); 
				if(isset($_SESSION["a"]) && isset($_SESSION["b"]) && isset($_POST["captcha"])){
					$resp = $_SESSION["a"]+$_SESSION["b"]; 
					$captcha = $_POST["captcha"]; 
					if($resp == $captcha){
						echo "Captcha Correcto <br>"; 
						for ($i=0; $i<4; $i++) {
							echo $_SESSION["consulta"][$i."a"]["1"];echo "<br>"; 
						}
						echo "<br> Precio Total: ".$_SESSION["total"];    
					}else{
						echo "Captcha Incorrecto";
					}
				}
			?>
			</main>
		</div>
	
		<footer class="main-footer">
			<p>Carrito de Compras - Todos los derechos reservados</p>
			<div class="siguenos"><a class="icon-facebook" href="http://facebook.com">Siguenos en facebook</a><a class="icon-twitter" href="http://twitter.com">Siguenos en Twitter</a><a class="icon-youtube" href="http://youtube.com">Siguenos en YouTube</a></div>
		</footer>
	</div>
</body>
</html>

