<!DOCTYPE html>
<html>
<head>
	<title>CARRITO DE COMPRAS </title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/font-awesome/css/fontawesome-all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
</head>
<body>

	<div class="contenedor">
		
	<!--INICIO Menu de Navegacion-->
    <nav class="iconos">
       	<ul>
       		<li>
				<a href="index.html">
					<div class="icono">
						<i class="fas fa-home"></i> <!--Etiqueta para icono-->
					</div>
					<div class="texto">
						Inicio
						<span>Página Principal</span>
					</div>
				</a>
			</li>
			<li>
				<a href="carritoC.php">
					<div class="icono">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="texto">
						Tienda Virtual
                        <span>Adquire nuestros artículos</span>
                    </div>
				</a>
			</li>
       	 	<li>
				<a href="comprobante.php">
					<div class="icono">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <div class="texto">
                        Ver Carrito
                        <span>Tus productos</span>
                    </div>
				</a>
			</li>
			<li>
				<a href="#">
					<div class="icono">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="texto">
                        Asistencia
                        <span>¿Necesitas Ayuda?</span>     
                    </div>
				</a>
			</li>
			<li>
				<a href="#">
					<?php session_start();
						if(isset($_SESSION["total"])){
							echo $_SESSION["total"];
						}
					?>
				</a>
			</li>
       	</ul>
	</nav>
	<!--FIN Menu de Navegacion-->

    <section>
		<?php
			$va = 0;
			$aux = array();
       
			$vect = array(array("img/laptop1.jpg"," LENOVO <br><br> camara web : si <br> disco duro : 1TB <br> entrada HDMI :si <br> velocidad procesador: 1.6 GB <br> tamaño de pantalla : 15.62",2200),
						array("img/laptop2.jpg", "SAMSUMG<br><br> camara web : si <br> disco duro : 500 GB <br> entrada HDMI :si <br> velocidad procesador: 1.3 GB <br> tamaño de pantalla : 14.62  "    ,4000),
						array("img/laptop3.jpg", " MacBook<br><br> Air 13 1.6GHZ/8GB/128GB - Oro ",2050),
						array("img/laptop4.jpg", "<br> MacBook<br><br> Totalmente equipado para un mundo inalámbrico<br>Procesador Intel Core M de doble núcleo a 1,2 GHz<br>512 GB de almacenamiento flash<br>",2050),
			);
     	?>

      	<form action="carritoC.php" method="post">
	 		<div class="contenedor catalogo">
	  			<main class="contenido-principal">
      			<?php
					for($i=0; $i<4; $i++){?>
                		<article class="entrada">
			  				<img src="<?php echo $vect[$i][0];?>">
			  				<div class="contenido">
      							<p><?php echo $vect[$i][1];?></p>
      							<p><?php echo $vect[$i][2];?></p>
      							<input type="number" name=" <?php echo $i;?>"> 
			  					<input type="submit" name=" <?php echo $i."a";?>">
			  				</div>
			  			</article>
        			<?php }
				?>
				</main>
			</div>
        </form>
    </section>

	<?php
		//utilice _session para almacenar variables y no sepierdan cada ves que envie datos al formulario
		//luego envio el total de la compra a con $_SESSION["imagenes"] $_SESSION["total"] para que reciva el archivo solve.php
		if( ($_POST["0a"]) &&  ($_POST["0"])){
			$_SESSION["consulta"]["0a"]["0"] = $_SESSION["consulta"]["0a"]["0"]+$_POST["0"]*$vect[0][2];
			$_SESSION["consulta"]["0a"]["1"]= "<br>unidades :".$_POST["0"]." marca: ".$vect[0][1]." a   precio unidad: ".$vect[0][2]."<br>";	
		}
		if( ($_POST["1a"]) && ($_POST["1"])){
			$_SESSION["consulta"]["1a"]["0"]= $_SESSION["consulta"]["1a"]["0"]+$_POST["1"]*$vect[1][2];$b=$_POST[1];
			$_SESSION["consulta"]["1a"]["1"]="<br>unidades :".$_POST["1"]." marca:".$vect[1][1]." a   precio unidad: ".$vect[1][2]."<br>";
		}
		if( ($_POST["2a"]) &&  ($_POST["2"])){
			$_SESSION["consulta"]["2a"]["0"]= $_SESSION["consulta"]["2a"]["0"]+$_POST["2"]*$vect[2][2];$b=$_POST[2];
			$_SESSION["consulta"]["2a"]["1"]="<br>unidades :".$_POST["2"]." marca:".$vect[2][1]." a   precio unidad: ".$vect[2][2]."<br>";
		}
		if( ($_POST["3a"]) &&  ($_POST["3"])){
			$_SESSION["consulta"]["3a"]["0"]= $_SESSION["consulta"]["3a"]["0"]+$_POST["3"]*$vect[3][2];$b=$_POST[3];
			$_SESSION["consulta"]["3a"]["1"]="<br>unidades :".$_POST["3"]." marca:".$vect[3][1]." a   precio unidad: ".$vect[3][2]."<br>";
		}

		$sum = 0;
		for ($i=0; $i <4; $i++){ 
			$sum = $sum + $_SESSION["consulta"][$i."a"]["0"];
		}
		$_SESSION["imagenes"] =  $vect;
		$_SESSION["total"] = $sum;
	?>

		<footer class="main-footer">
			<p>Carrito de Compras - Todos los derechos reservados</p>
			<div class="siguenos"><a class="icon-facebook" href="http://facebook.com">Siguenos en facebook</a><a class="icon-twitter" href="http://twitter.com">Siguenos en Twitter</a><a class="icon-youtube" href="http://youtube.com">Siguenos en YouTube</a></div>
		</footer>
	</div>
</body>
</html>